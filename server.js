'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require("fs");
const Guid = require("guid");

//Json Responds files
const employees = require("./data/employees");
const stats = require("./data/statistic");
const rootAPI = '/api/v1';
const PORT = process.env.PORT || 3000;
console.log(process.version)
app.use(bodyParser.json());

// on Empty Path
app.get(rootAPI, function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

// on Empty Path
app.get('/', function (req, res) {
    console.log('Get All employees');
    res.send("Try /api/v1/employees")
});

// Get All Employees
app.get(rootAPI + '/employees', function (req , res) {
    console.log('Get All employees');
    fs.readFile('./data/employees.json', function (err, data) {
        let jsonArray = JSON.parse(data);
        if (jsonArray.length > 0) {
            let response = [];
            jsonArray.map(function(obj){
                response.push({
                    "id": obj.id,
                    "firstName": obj.firstName,
                    "lastName" : obj.lastName,
                    "email": obj.email
                })
            })
            res
                .status(200)
                .json(response);
        }
    });
    //TODO: This is probably working, but what if the json array has millions of records:
    // Try to make it synchronuous

});

//TODO  Get Statistic : Write the GET function to read from static json file and
// respond a json array including all the objects
app.get(rootAPI + '/statistic', function (req , res) {
    console.log('Get All statistic');
    fs.readFile('./data/statistic.json', function (err, data) {
        let jsonArray = JSON.parse(data);
        if (jsonArray.length > 0) {
            
            res
                .status(200)
                .json(jsonArray);
        }
    });

});

// Get one Employees on ID
app.get(rootAPI + '/employees/:id', function (req, res) {
    console.log('Get an employee based on id');
    // get the ID from url params which user has called
    const employeeId = String(req.params.id);

    new Promise(function (resolve, reject) {
        // reading the json file
        fs
            .readFile('./data/employees.json', function (err, data) {
                // parse as json
                let json = JSON.parse(data);
                // find the element
                json.find(function (employee, index) {
                    if (employee.id === employeeId) {
                        resolve(employee)
                    }
                    if (index === json.length - 1) {
                        reject()
                    }
                });

            });
    })
        .then(function (result) {
            res
                .status(200)
                .json(result);
        })
        .catch(function (err) {
            console.log("error:" + err);
            res
                .status(404)
                .end();
        })
});

app.post(rootAPI + '/employees', function (req, res) {
    // We are checking that call includes all the properties
if (req.body.firstName && req.body.lastName && req.body.email && req.body.gender && Number(req.body.age) && String(req.body.startDate) && req.body.image) {
    // We read the file and parse the json
    fs
        .readFile('./data/employees.json', function (err, data) {
            let json = JSON.parse(data);
             // create the object to push to json array
            let objectToPush = {
                "id": Guid.raw(),
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "email": req.body.email,
                "gender": req.body.gender,
                "age": Number(req.body.age),
                "startDate": String(req.body.startDate),
                "image": req.body.image
            }
            json.push(objectToPush)
            // after pushing, we write the whole json array to file again and then we respond 200
            fs.writeFile('./data/employees.json', JSON.stringify(json), function (err) {
                if (err) 
                    console.log(err);
                res
                    .status(200)
                    .json(objectToPush);
            })

        })
} else {
    res.send("Please complete all the required fields : firstName, lastName, email, gender, ag" +
            "e(Number), startDate(epoch), image")
    res
        .status(404)
        .end();
    return;
}
});


//Delete an Employee
app.delete(rootAPI + '/employees/:id', function (req, res) {
    // we get the id from url
    const employeeId = String(req.params.id);
    console.log('Delete employee: ' + employeeId);
    // we read the file as a promise
    new Promise(function (resolve, reject) {
        fs
            .readFile('./data/employees.json', function (err, data) {
                var json = JSON.parse(data);
                 // filter function will remove the object (return all except that one) which has the same id as the id we got from url params
                var jsonNew = json.filter(function(e) {
                    return e.id !== employeeId
                });
                // we check if we have the same lenght as first, it means that we have not found the id
                if (json.length === jsonNew.length) {
                    resolve(false)
                } else {
                    resolve(jsonNew)
                }

            });
    })
        .then(function (result) {
            if (!result) {
                return res
                    .status(404)
                    .send("id not found")
                    .end();

            }
            fs.writeFile('./data/employees.json', JSON.stringify(result), (err) => {
                if (err) {
                    throw err;
                }
                return res
                    .status(200)
                    .send('Deleted Successfully');

            });

        })
        .catch(function (err) {
            return res
                .status(404)
                .end();
        })

});

//update elements of one Employees by ID
app.put(rootAPI + '/employees/:id', function (req, res) {
    console.log('update an employee based on id');
    const employeeId = String(req.params.id);
    const payload = req.body;
    new Promise(function (resolve, reject) {
        fs
            .readFile('./data/employees.json', function (err, data) {
                let json = JSON.parse(data);
                json.find(function (employee, index) {
                    if (employee.id === employeeId) {
                        for(var name in payload){
                            employee[name] = payload[name];
                        }
                        json.push(employee)
                        resolve(json);
                    }
                    if (index === json.length - 1) {
                        reject()
                    }
                });

            });
        })
        .then(function (result) {
            fs.writeFile('./data/employees.json', JSON.stringify(result), (err) => {
                if (err) {
                    throw err;
                }
                res
                .status(200)
                .json(result);

            });
            
        })
        .catch(function (err) {
            res
                .status(404)
                .end();
        })

});



// object in the json file Running API Server
var serverHost = app.listen(PORT, function () {
    const host = serverHost
        .address()
        .address;
    const port = serverHost
        .address()
        .port;
    console.log('Backend running on ' + host + ':' + port);
});